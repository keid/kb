# Keid Engine

> Desktop-only, [vulkan-only](Introduction/Why.md) and mostly Haskell.

![](keid.png)

- https://gitlab.com/keid — Project umbrella group.
- https://gitlab.com/keid/engine — Engine packages (available on hackage).
  + [keid-core]: Core parts of Keid engine.
  + [keid-geometry]: Geometry primitives.
  + [keid-render-basic]: Basic rendering programs.
  + [keid-resource-gltf]: GLTF loader.
  + [keid-sound-openal]: OpenAL sound system.
  + [keid-ui-dearimgui]: Immediate mode UI with `dear-imgui`.
  + [keid-frp-banana]: Event/behavior FRP with `reactive-banana`.
- https://gitlab.com/keid/tools
  + [weld](https://gitlab.com/keid/tools/weld): Asset processing tool.
- https://gitlab.com/keid/examples
  + [overview](https://gitlab.com/keid/examples/overview): Literate single-file example 👈
  + [2d/texts-and-sprites](https://gitlab.com/keid/examples/2d/texts-and-sprites): CPU particle system demo that can be used as a benchmark.
  + [2d/tiles](https://gitlab.com/keid/examples/2d/tiles): Texture-packed tilemap rendering.
  + [headless](https://gitlab.com/keid/examples/headless): Non-interactive rendering to a file.
  + [shader/external](https://gitlab.com/keid/examples/shader/external): External shader code with hot reloading.
  + [sound/positional](https://gitlab.com/keid/examples/sound/positional): Positional sound with OpenAL.
  + [ui/dearimgui](https://gitlab.com/keid/examples/ui/dearimgui): Widget demo and some GLFW event handling gotchas.
- https://gitlab.com/keid/demos
  + [frp](https://gitlab.com/keid/demos/frp): Using FRP libraries (only `reactive-banana` for now, PRs welcome).
  + [pbr](https://gitlab.com/keid/demos/pbr): Physically-based rendering with UI-tweakable materials.
  + [sky-playground](https://gitlab.com/keid/demos/sky-playground): Sandbox for feature experiments.
  + [snap](https://gitlab.com/keid/demos/snap): 3D tile editor.
- https://gitlab.com/keid/kb — This site.
- https://gitlab.com/keid/meta — A project-management project for the group.

[keid-core]: https://hackage.haskell.org/package/keid-core/
[keid-frp-banana]: https://hackage.haskell.org/package/keid-frp-banana/
[keid-geometry]: https://hackage.haskell.org/package/keid-geometry/
[keid-render-basic]: https://hackage.haskell.org/package/keid-render-basic/
[keid-resource-gltf]: https://hackage.haskell.org/package/keid-resource-gltf/
[keid-sound-openal]: https://hackage.haskell.org/package/keid-sound-openal/
[keid-ui-dearimgui]: https://hackage.haskell.org/package/keid-ui-dearimgui/

## Games

- [Orboros](https://gitlab.com/dpwiz/orboros) — LD47, playable. Ported from vulkan-setup.
- [Time to think](https://gitlab.com/dpwiz/time-to-think) — LD48, unfinished. New code upstreamed to engine and reused in Orboros.
- [Swerve](https://gitlab.com/dpwiz/swerve) — LD49, playable.

## Installation

There are some binary dependencies:

- Platform libs (apt install `build-essential libtinfo-dev` or something).
- Vulkan SDK [from LunarG](https://vulkan.lunarg.com/sdk/home).
- GLFW library (`libglfw3-dev`).
  - May require extra platform deps like `libxinerama-dev libxxf86vm-dev libxi-dev libxcursor-dev`.
- OpenAL library (`libopenal-dev`) and Opus codec (`libopusfile-dev`).

Most of the packages are published on Hackage, you don't need to clone all the repositories here.

But you may need to pin all the versions:

- Stack resolvers: https://gitlab.com/keid/meta/-/tree/main/resolvers
- Cabal freeze is generated from most recent resolver: https://gitlab.com/keid/meta/-/blob/main/cabal.project.freeze

Look at game project files for configuration example, those should work with both `stack` and `cabal` builds.

> Some problems may arise in Nix setups, but I don't know anything about it. You'd better consult your local Nix guru.

## Contacts

- [matrix channel](https://matrix.to/#/#keid:haskell-game.dev)
- [gitlab issues](https://gitlab.com/groups/keid/-/issues)
- https://haskell-game.dev/#where-to-ask
