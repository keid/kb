# Cube maps

The engine uses the following cube face order: right left top bottom front back.

----

> Cubemap resource information moved to a general page on [texture compression](Texture-compression.md) page.
