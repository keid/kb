---
title: 'Why?'
---

## A quest for a modern «full-stack» game engine

Haskell has a deep history of game development, with many options catering to different skill levels.
However, getting better and outgrowing your current toolkit results in a series of "restart from scratch" experiences.
That's bad for learning and injects even more points of failure into the engagement funnel.

Currently, a roadmap for some zero-programming folk could look like this:

0. Code World

    Nothing to install, just start coding and watch the results.

    It has some advanced features, but usually one would want a shippable thing and some degree of project persistence.

1. Gloss

    Toolchain and dependency installation is required, but it's a smooth sail afterward.

    With its assortment of "main" wrappers and access to the whole Hackage, it allows exploring many game- and engine-related concepts without leaving the comfort of its rendering engine.

    A solid pick for vector graphics, but bulks at texts and texture rendering.
    This is a pity since many consider those aspects fundamental.
    Especially in the roguelike community, which provides a steady trickle of new users.

2. SDL

    A whole new world of windowing, events, and blitting, together with a bunch of companion subsystem packages.

    Coming from Gloss you'll find yourself lacking any structural support and have to assemble a simple rendering engine.
    Despite far from being rocket science it's an error-prone and sometimes tedious task.

    You may have to learn some FFI or a thing about pointers here.

    But what if you need more control, more advanced effects, or 3D geometry?

3. OpenGL

    A rendering substrate for both Gloss and SDL (on Linux), masterfully hidden from sight and now exposed.

    Dive right into the history of rendering, trawl the Web for scene-building tutorials and a trove of special effect techniques. Albeit some of them would be in another shader language or just pure math.

    Here, it may take a lot of effort to sort out ancient and inefficient from modern and awesome.

    You will find, eventually, that OpenGL is "done" as an API for more than 5 years already.
    It is still possible to write modern OpenGL, but it is merely a shadow of its successor.

4. Vulkan

    If coming from Gloss to OpenGL was "getting to know how the sausage is made" then Vulkan is not too far from deriving meat from protein structure.

    Suddenly, there's nothing under you, just hardware, represented with bytes, bytecode, pointers, descriptors, semaphores...
    But almost none of the mutable state or implicit effects.
    The things you've assembled yourself stay exactly as they are, crashing and freezing as you told them to do.

    There are a lot fewer tutorials to trawl and some old hardware and drivers lack proper support.
    Finally, there is a solid ground for your abstractions.

The good thing is one can get in at any level, matching their competence.
But unless you're satisfied you've got few options, all unpleasant:

- Shift gears and retrain.
- Flee gamedev and switch to writing Haskell somewhere else.
- Abandon Haskell. A Game Over for our community.

A solution, thus, is to have all of the levels available in a single modular engine.

Faced with a need for something you can't have at your current rung you will reach into its implementation.

Found a good solution for a particular task?
Upstream or otherwise publish your findings, growing the ecosystem.
